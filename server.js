const express = require('express')
const bodyParser = require('body-parser')
const dotenv = require('dotenv');
dotenv.config({ path: './Environment/.env' })
const session = require('express-session');
const cors = require('cors')
const production = process.env.production
const routeIndex = require('./app/Routes/index')
const app = express()

app.use(bodyParser.json())

const PORT = process.env.PORT || 3000;
const CORS_ORIGIN = process.env.cors_origin || ""
app.use(cors({
    credentials: true,
    origin: CORS_ORIGIN
}));


if (!production) {
    app.use(morgan('dev'));
}


// Routes
app.use('/api', routeIndex)


app.listen(PORT, () => {

    console.log('App started with cors enabled');
});
