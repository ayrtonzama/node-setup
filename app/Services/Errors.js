const createError = require('http-errors')

exports.errorResponseUnexpected=(code,message)=>{
    return createError(code,message)
}

exports.errorNotFound=()=>{
    return createError.NotFound();
}

exports.errorUnauthorized=()=>{
    return createError.Unauthorized();
}

exports.errorValidation=(data)=>{
    return createError.UnprocessableEntity(data.toString())
}

exports.errorServer=()=>{
    return createError.InternalServerError();
}