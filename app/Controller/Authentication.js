const dotenv = require('dotenv');
dotenv.config({ path: '../../Environment/.env' })
const bcrypt = require('bcryptjs');
const { errorResponseUnexpected, errorNotFound, errorUnauthorized,
    errorValidation,
    errorServer } = require("../Services/Errors")
// Login function
exports.login = async (req, res, next) => {
    try {

    } catch (Exception) {
        return errorResponseUnexpected(400,Exception);
    }
}

// Logout function
exports.logout = async (req, res, next) => {
    try {

    } catch (Exception) {
        return errorResponseUnexpected(400,Exception);
    }
}

// Register function
exports.register = async (req, res, next) => {
    try {

    } catch (Exception) {
        return errorResponseUnexpected(400,Exception);
    }
}

exports.forgotPassword = async (req, res, next) => {
    try {

    } catch (Exception) {
        return errorResponseUnexpected(400,Exception);
    }
}